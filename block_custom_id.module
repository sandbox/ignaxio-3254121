<?php

/**
 * @file
 * Adding id to blocks.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\block\Entity\Block;
use Drupal\Component\Utility\Html;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\block\BlockInterface;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityFormInterface;

/**
 * Implements hook_help().
 */
function block_custom_id_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the forms_to_email module.
    case 'help.page.block_custom_id':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t("Block Class allows users to add classes to any block through the block's configuration interface. Hooray for more powerful block theming!") . '</p>';

      $output .= '<h3>' . t('Installation note') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Enable the module on <a href=":extend_link">extend menu</a>.', [':extend_link' => Url::fromRoute('system.modules_list')->toString()]) . '</dt>';
      $output .= '</dl>';

      $output .= '<h3>' . t('Usage') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t("To add a class to a block, simply visit that block's configuration page at Administration > Structure > Block Layout and click on Configure of the desired block.") . '</dt>';
      $output .= '</dl>';

      return $output;
  }
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function block_custom_id_block_presave(BlockInterface $entity) {
  if (empty($entity->getThirdPartySetting('block_custom_id', 'id'))) {
    $entity->unsetThirdPartySetting('block_custom_id', 'id');
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function block_custom_id_form_block_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if (\Drupal::currentUser()->hasPermission('administer block classes')) {
    $form_object = $form_state->getFormObject();
    if ($form_object instanceof EntityFormInterface) {
      /** @var \Drupal\block\BlockInterface $block */
      $block = $form_object->getEntity();

      // This will automatically be saved in the third party settings.
      $form['third_party_settings']['#tree'] = TRUE;
      $form['third_party_settings']['block_custom_id']['id'] = [
        '#type' => 'textfield',
        '#title' => t('Block id'),
        '#description' => t('Setting the block id.'),
        '#default_value' => $block->getThirdPartySetting('block_custom_id', 'id'),
        '#maxlength' => 255,
      ];
    }
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function block_custom_id_preprocess_block(&$variables) {
  // Blocks coming from page manager widget does not have id.
  if (!empty($variables['elements']['#id'])) {
    $block = Block::load($variables['elements']['#id']);
    if ($block && $id = $block->getThirdPartySetting('block_custom_id', 'id')) {
      $variables['attributes']['id'] = $id;
    }
  }
}
